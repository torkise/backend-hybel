from django.urls import path

from . import views

app_name = "hybel-app"
urlpatterns = [
    path('view/', views.GetHybel.as_view()),
    path('create/', views.AddHybel.as_view()),
]
