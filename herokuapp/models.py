from django.db import models

class Hybel(models.Model):
     
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    pris = models.IntegerField(default=0)
    antallSoverom = models.IntegerField(default=0)
    antallLedigeSoverom = models.IntegerField(default = 1)
    beskrivelse = models.TextField(default = "")
    adresse = models.TextField(default="")
    epost = models.TextField(default="")