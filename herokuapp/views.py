from wsgiref import headers
from django.shortcuts import render
from .models import Hybel
from .serializers import HybelSerializer
from rest_framework.generics import ListAPIView, CreateAPIView

class AddHybel(CreateAPIView):
    queryset=Hybel.objects.all()
    serializer_class = HybelSerializer

class GetHybel(ListAPIView):
    queryset = Hybel.objects.all()
    serializer_class = HybelSerializer
